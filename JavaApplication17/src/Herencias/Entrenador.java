/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencias;

/**
 *
 * @author estudiante
 */
public class Entrenador extends EquipoFutbol{
    int idFederacion;

    public Entrenador(int idFederacion, String nombre, String apellido, int id, int edad) {
        super(nombre, apellido, id, edad);
        this.idFederacion = idFederacion;
    }

    public int getIdFederacion() {
        return idFederacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getId() {
        return id;
    }

    public int getEdad() {
        return edad;
    }
    
    public void dirigirPartido(){
        System.out.println("Dirige el partido");
    }
    
}
