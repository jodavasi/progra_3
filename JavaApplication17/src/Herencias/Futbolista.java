/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencias;

/**
 *
 * @author estudiante
 */
public class Futbolista extends EquipoFutbol{
    
    public int dorsal;
    public  String posicion;
    
    public Futbolista(String nombre, String apellido, int id, int edad, int dorsal,String posicion) {
        super(nombre, apellido, id, edad);
        this.dorsal = dorsal;
        this.posicion = posicion;
        
    }

    public int getDorsal() {
        return dorsal;
    }

    public String getPosicion() {
        return posicion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getId() {
        return id;
    }

    public int getEdad() {
        return edad;
    }
    
    public void jugarPartido(){
        System.out.println("Va a jugar");
    }
    
    public void entrenar(){
        System.out.println("Va a entrenar");
    }
    
}
