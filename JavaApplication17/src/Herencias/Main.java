/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencias;

import javax.swing.UIManager;

/**
 *
 * @author estudiante
 */
public class Main {
    public static void main(String[] args) {
        Futbolista j1 = new Futbolista("Daniel", "Vargas", 207610854, 22, 9, "delantero");
        Entrenador e1 = new Entrenador(2911, "Alejandro", "Alfaro", 30,202220222);
        Masajista m1 = new Masajista("Master", 10, "Weslin", "Medina", 23, 203330212);
        System.out.println(j1.getNombre());
        System.out.println(j1.getApellido());
        System.out.println(j1.getDorsal());
        System.out.println(j1.getEdad());
        System.out.println(j1.getId());
        System.out.println(j1.getPosicion());
        System.out.println("\n");
        System.out.println(e1.getNombre());
        System.out.println(e1.getApellido());
        System.out.println(e1.getId());
        System.out.println(e1.getEdad());
        System.out.println(e1.getIdFederacion());
        System.out.println("\n");
        System.out.println(m1.getNombre());
        System.out.println(m1.getApellido());
        System.out.println(m1.getEdad());
        System.out.println(m1.getTitulacion());
        System.out.println(m1.getAniosExp());
    }
}
