/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencias;

/**
 *
 * @author estudiante
 */
public class Masajista extends EquipoFutbol {
    public String titulacion;
    public int aniosExp;

    public Masajista(String titulacion, int aniosExp, String nombre, String apellido, int id, int edad) {
        super(nombre, apellido, id, edad);
        this.titulacion = titulacion;
        this.aniosExp = aniosExp;
    }

    public String getTitulacion() {
        return titulacion;
    }

    public int getAniosExp() {
        return aniosExp;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getId() {
        return id;
    }

    public int getEdad() {
        return edad;
    }
    
    public void darMasaje(){
        System.out.println("Da el masaje a jugadores");
    }
}
