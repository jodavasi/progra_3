/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaHerenciaProductos;

/**
 *
 * @author estudiante
 */
public class Refrigerados extends Productos{
    public int codOrSupAl,tempManR;
    public String d,m,a,fEnvasado,pOrigen;

    public Refrigerados(int codOrSupAl, int tempManR, String d, String m, String a, String fEnvasado, String pOrigen, int fechaCad, int nLote) {
        super(fechaCad, nLote);
        this.codOrSupAl = codOrSupAl;
        this.tempManR = tempManR;
        this.d = d;
        this.m = m;
        this.a = a;
        fEnvasado +=d+"-";
        fEnvasado +=m+"-";
        fEnvasado +=a;
        this.fEnvasado = fEnvasado;
        this.pOrigen = pOrigen;
    }
    
    public String gerAtributos(){
        return "Fecha vencimiento:"+fechaCad
                +"\n Numero de lote:"+nLote
                +"\n Fecha envasado: "+fEnvasado
                +"\nPais de origen: "+pOrigen
                +"\nCodigo del organismo de supervision alimentaria: "+codOrSupAl
                +"\nTemperatura de mantenimiento recomendada en grados Celcius"+tempManR;
    }
    
}
