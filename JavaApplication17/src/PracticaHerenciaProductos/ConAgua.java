/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaHerenciaProductos;

/**
 *
 * @author estudiante
 */
public class ConAgua extends Congelados{
    public int gSal,lAgua,sanidadAgua;
    

    public ConAgua(int gSal, int lAgua, int sanidadAgua, String d, String m, String a, String fEnvasado, String pOrigen, String temManRe, int fechaCad, int nLote) {
        super(d, m, a, fEnvasado, pOrigen, temManRe, fechaCad, nLote);
        this.gSal = gSal;
        this.lAgua = lAgua;
        sanidadAgua = gSal*lAgua; 
        this.sanidadAgua = sanidadAgua;
    }
    
    public String getAtributos(){
        return "Fecha vencimiento:"+fechaCad
                +"\n Numero de lote:"+nLote
                +"\n Fecha envasado: "+fEnvasado
                +"\nPais de origen: "+pOrigen
                +"\nTemperatura de mantenimiento recomendada en grados Celcius"+temManRe
                +"\nSanidad del agua: "+sanidadAgua+"%";
    }    
}
