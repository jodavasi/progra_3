/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaHerenciaProductos;

/**
 *
 * @author estudiante
 */
public class Congelados extends Productos{
   public String d,m,a,fEnvasado,pOrigen,temManRe;

    public Congelados(String d, String m, String a, String fEnvasado, String pOrigen, String temManRe, int fechaCad, int nLote) {
        super(fechaCad, nLote);
        this.d = d;
        this.m = m;
        this.a = a;
        fEnvasado +=d+"-";
        fEnvasado +=m+"-";
        fEnvasado +=a;
        this.fEnvasado = fEnvasado;
        this.pOrigen = pOrigen;
        this.temManRe = temManRe;
    }
    
    public String getAtributos(){
        return "Fecha vencimiento:"+fechaCad
                +"\n Numero de lote:"+nLote
                +"\n Fecha envasado: "+fEnvasado
                +"\nPais de origen: "+pOrigen
                +"\nTemperatura de mantenimiento recomendada en grados Celcius"+temManRe;
    }
}
