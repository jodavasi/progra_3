/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaHerenciaProductos;

/**
 *
 * @author estudiante
 */
public class ConNitrogeno extends Congelados{
    public String metCongelacion;
    public int tCongelacionSec;

    public ConNitrogeno(String metCongelacion, int tCongelacionSec, String d, String m, String a, String fEnvasado, String pOrigen, String temManRe, int fechaCad, int nLote) {
        super(d, m, a, fEnvasado, pOrigen, temManRe, fechaCad, nLote);
        this.metCongelacion = metCongelacion;
        this.tCongelacionSec = tCongelacionSec;
    }
    
    public String getAtributos(){
        return "Fecha vencimiento:"+fechaCad
                +"\n Numero de lote:"+nLote
                +"\n Fecha envasado: "+fEnvasado
                +"\nPais de origen: "+pOrigen
                +"\nTemperatura de mantenimiento recomendada en grados Celcius"+temManRe
                +"\nMetodo de congelacion: "+metCongelacion
                +"\nTiempo de congelacion por segundos: "+tCongelacionSec;
    }
    

}

    
    
    
    
    

