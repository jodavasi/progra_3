/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaHerenciaProductos;

import java.awt.List;
import java.util.ArrayList;

/**
 *
 * @author estudiante
 */
public class Frescos extends Productos{
    
    public String d,m,a,fEnvasado,paisOrigen;
    public ArrayList<String> frescos; 

    public Frescos(String d, String m, String a, String fEnvasado, String paisOrigen, int fechaCad, int nLote) {
        super(fechaCad, nLote);
        this.d = d;
        this.m = m;
        this.a = a;
        fEnvasado +=d+"-";
        fEnvasado +=m+"-";
        fEnvasado +=a;
        this.fEnvasado = fEnvasado;
        this.paisOrigen = paisOrigen;
        
        
    }
    
    public String getAtributos(){
        return "Fecha vencimiento:"+fechaCad
                +"\n Numero de lote:"+nLote
                +"\n Fecha envasado: "+fEnvasado
                +"\nPais de origen: "+paisOrigen;
    }
}
