/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PracticaHerenciaProductos;

/**
 *
 * @author estudiante
 */
public class ConAire extends Congelados{
        public String nitro,oxigeno,cO2,vAgua;

    public ConAire(String nitro, String oxigeno, String cO2, String vAgua, String d, String m, String a, String fEnvasado, String pOrigen, String temManRe, int fechaCad, int nLote) {
        super(d, m, a, fEnvasado, pOrigen, temManRe, fechaCad, nLote);
        this.nitro = nitro+"%";
        this.oxigeno = oxigeno+"%";
        this.cO2 = cO2+"%";
        this.vAgua = vAgua+"%";
    }
    
    public String getAtributos(){
        return "Fecha vencimiento:"+fechaCad
                +"\n Numero de lote:"+nLote
                +"\n Fecha envasado: "+fEnvasado
                +"\nPais de origen: "+pOrigen
                +"\nTemperatura de mantenimiento recomendada en grados Celcius"+temManRe
                +"\nNitrogeno: "+nitro
                +"\nOxigeno: "+oxigeno
                +"\nCO2: "+cO2
                +"\nVapor de Agua: "+vAgua;
                
    }
}

